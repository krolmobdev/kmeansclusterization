package kmeansclusterization;

import java.util.*;

public class KMeansAlgorithm {

    private int mNumClusters;//число кластеров
    private int mIterationCount;//число итераций
    private int[] mClusteringResult;//результат кластеризации
    private double[][] mMeans;//центроиды
    private double[][] mRawData;//кластеризуемые данные

    /**
     * Возвращает число кластеров
     */
    public int getNumClusters() {
        return mNumClusters;
    }

    /**
     * Возвращает результат кластеризации
     *
     * @return Массив, содержащий распределение данных по кластерам: индекс массива-номер элемента данных,элемент-номер кластера,к которому относится элемент
     */
    public int[] getClusteringResult() {
        return mClusteringResult;
    }

    /**
     * Возвращает кластеризованные данные
     */
    public List<List<double[]>> getClusteredData() {
        List<List<double[]>> clusters = new ArrayList<List<double[]>>();

        for (int i = 0; i < mNumClusters; i++) {
            clusters.add(new ArrayList<double[]>());
        }

        for (int i = 0; i < mClusteringResult.length; i++) {
            clusters.get(mClusteringResult[i]).add(mRawData[i]);
        }

        return clusters;
    }

    /**
     * Возвращает центроиды
     */
    public double[][] getMeans() {
        return mMeans;
    }

    /**
     * Возвращает кластеризуемые данные
     */
    public double[][] getRawData() {
        return mRawData;
    }

    /**
     * Возвращает число итераций алгоритма
     */
    public int getIterationCount() {
        return mIterationCount;
    }

    /**
     * Возвращает результат работы алгоритмв
     */
    public String getProtocol() {
        String res = "";
        StringBuilder sb = new StringBuilder(res);
        sb.append("Число кластеров:");
        sb.append(mNumClusters);
        sb.append('\n');
        sb.append("Число итераций:");
        sb.append(mIterationCount);
        sb.append('\n');

        List<List<double[]>> clusteredData = getClusteredData();
        for (int i = 0; i < mNumClusters; i++) {
            sb.append("Кластер № ");
            sb.append(i);
            sb.append("\nЦентроид:\n");
            sb.append(Arrays.toString(mMeans[i]));
            sb.append("\nДанные:");

            for (double[] item : clusteredData.get(i)) {
                sb.append('\n');
                sb.append(Arrays.toString(item));
            }

            sb.append('\n');
        }

        return sb.toString();
    }

    /**
     * Создает алгоритм к-средних
     *
     * @param numClusters Число кластеров
     * @param rawData     Кластеризуемые данные
     */
    public KMeansAlgorithm(int numClusters, double[][] rawData) {
        mNumClusters = numClusters;

        if (rawData == null) {
            throw new NullPointerException("Данные mData для кластеризации не заданы");
        }
        mRawData = rawData;

        mMeans = new double[numClusters][];
        mClusteringResult = new int[rawData.length];
    }

    /**
     * Кластеризация по алгориму к-средних
     */
    public void cluster() {

        double[][] data = mRawData;

        boolean changed = true; // выполнено ли назначение данных хотябы одному кластеру?
        boolean success = true; // заполнены ли все кластеры (отсутсвуют кластеры с нулевым числом элементов)

        mMeans = initMeans(mNumClusters, mRawData); // задание начальных значений центроидам

        int maxCount = data.length * 10; // максимально возможное число итераций алгоритма к-средних
        int ct = 0;
        while (changed && success && ct < maxCount) {
            changed = updateClustering(mRawData, mClusteringResult, mMeans); // назначение данных кластерам
            success = updateMeans(mRawData, mClusteringResult, mMeans); // вычисление новых значений центроидов
            ++ct;
        }
        mIterationCount = ct;

        // Здесь может быть проверка на наличие пустых кластеров
    }

    /**
     * Присваивает начальные значения центроидам.
     * Выбираем к-значений данных в качестве начальных значений центроидов:
     * выбираем случайное значение центроида для первого кластера
     * для оставшихся кластеров:
     * вычисляем квадратичное расстоние между каждым элементом данных для нахождения наименьшего квадратичного расстояния
     * выбераем элемент данных/наибольшее квадратичное расстоние как следующее среднее значение
     *
     * @param numClusters Число кластеров
     * @param data        Данные для кластеризации
     */
    private double[][] initMeans(int numClusters, double[][] data) {

        double[][] means = makeMatrix(numClusters, data[0].length); // результат

        List<Integer> used = new ArrayList<Integer>(); // отслеживает, какие элементы уже назначены кластерам

        // Выбирает индекс одного элемента данных случайным образом
        Random rnd = new Random();
        int idx = rnd.nextInt(data.length); // [0, data.Length-1]
        means[0] = Arrays.copyOf(data[idx], data[idx].length);
        used.add(idx); // мы не можем использовать выбранный элемент снова

        for (int k = 1; k < numClusters; ++k) { // для каждого оставшегося кластера
            double[] dSquared = new double[data.length]; // ближайшее среднее
            int newMean = -1; // индекс элемента данных будет новым центроидом
            for (int i = 0; i < data.length; ++i) {
                // если элемент данных уже использован, то пропускаем его
                if (used.contains(i)) continue;

                // вычисляем расстояние между каждым элементом данных и центроидом (для нахождения наименьшего)
                double[] distances = new double[k]; // имеем к-средних значений
                for (int j = 0; j < k; ++j)
                    distances[j] = distance(data[i], means[k]);

                // получем индекс ближайщего среднего
                int m = minIndex(distances);
                //вычисляем связанное с индексом ближайшего среднего квадратичное расстояние
                dSquared[i] = distances[m] * distances[m];
            }
            //Выбираем один из элементов данных используя квадратичное расстояние
            // Для выбора используется алгоритм колеса рулетки
            double p = rnd.nextDouble();
            double sum = 0.0; // сумма квадратичных расстояний
            for (int i = 0; i < dSquared.length; ++i) {
                sum += dSquared[i];
            }
            double cumulative = 0.0; // коммулятивная вероятность

            int ii = 0; // points into distancesSquared[]
            int sanity = 0; // количество сантиметров
            while (sanity < data.length * 2) { // 'stochastic acceptance'
                cumulative += dSquared[ii] / sum;
                if (cumulative >= p && !used.contains(ii)) {
                    newMean = ii; // выбраный индекс
                    used.add(newMean); // не выбирать этот индекс снова
                    break;
                }
                ++ii; // следующий кандидат
                if (ii >= dSquared.length) ii = 0; // вернуться к первому элементу данных
                ++sanity;
            }
            // проверка если  newMean до сих пор -1 . . .

            // сохраняем данные выбранного индекса
            means[k] = Arrays.copyOf(data[newMean], data[newMean].length);
        }
        return means;
    }

    /**
     * Выполняет нормализацию данных для кластеризации
     * по формуле (элемент-среднее значение данных)/среднеквадратичное отклонение данных
     */
    private double[][] normalized(double[][] data) {
        // Копия выходных данных
        double[][] result = new double[data.length][];

        for (int i = 0; i < data.length; ++i) {
            result[i] = new double[data[i].length];
            result[i] = Arrays.copyOf(data[i], data[i].length);
        }

        for (int j = 0; j < result[0].length; ++j) {
            double colSum = 0.0;
            for (int i = 0; i < result.length; ++i) {
                colSum += result[i][j];
            }

            double mean = colSum / result.length;

            double sum = 0.0;
            for (int i = 0; i < result.length; ++i) {
                sum += (result[i][j] - mean) * (result[i][j] - mean);
            }

            double sd = sum / result.length;
            for (int i = 0; i < result.length; ++i) {
                result[i][j] = (result[i][j] - mean) / sd;
            }
        }
        return result;
    }

    /**
     * Создает матрицу для метода cluster
     *
     * @param rows Число строк
     * @param cols Число столбцов
     */
    private double[][] makeMatrix(int rows, int cols) {
        double[][] result = new double[rows][];
        for (int i = 0; i < rows; ++i) {
            result[i] = new double[cols];
        }
        return result;
    }

    /**
     * Обновляет значения центроидов
     *
     * @param data       Данные для кластеризациии
     * @param clustering Распределение данных по кластерам
     * @param means      Центроиды
     * @return true - в случае успешной кластеризации, false - если есть кластер с неназначенными кортежами
     */
    private boolean updateMeans(double[][] data, int[] clustering, double[][] means) {
        // Проверка числа элементов каждого кластера
        // Можно пропустить эту проверку, если методы initClustering и updateClustering
        // оба гарантируют по крайней мере один кортеж в каждом кластере (обычно это правда)
        int numClusters = means.length;
        int[] clusterCounts = new int[numClusters];
        for (int i = 0; i < data.length; ++i) {
            int cluster = clustering[i];
            ++clusterCounts[cluster];
        }

        for (int k = 0; k < numClusters; ++k) {
            if (clusterCounts[k] == 0)
                return false; // нет изменения значения центроидов (means)
        }

        for (int k = 0; k < means.length; ++k) {
            for (int j = 0; j < means[k].length; ++j) {
                means[k][j] = 0.0;
            }
        }

        for (int i = 0; i < data.length; ++i) {
            int cluster = clustering[i];
            for (int j = 0; j < data[i].length; ++j) {
                means[cluster][j] += data[i][j]; // сумма элементов j-го кластера
            }
        }

        for (int k = 0; k < means.length; ++k) {
            for (int j = 0; j < means[k].length; ++j) {
                means[k][j] /= clusterCounts[k]; // возможно деление на ноль
            }
        }
        return true;
    }

    /**
     * Выполняет кластеризацию:
     * назначает каждый элемент данных кластеру
     * (используя принцип наименьшего расстояния между элементом и центроидом кластера)
     *
     * @param data       Данные для кластеризации
     * @param clustering Распределение данных по кластерам
     * @param means      Центроиды
     * @return true - в случае успешной кластеризации,
     * false - если назначения кортежей не изменяются или
     * если переназначение приведет к кластеризации, где в одном или нескольких кластерах нет кортежей
     */
    private boolean updateClustering(double[][] data, int[] clustering, double[][] means) {
        int numClusters = means.length;
        boolean changed = false;

        int[] newClustering = new int[clustering.length]; // результат кластеризации
        newClustering = Arrays.copyOf(clustering, clustering.length);

        // Расстояния между выбранным элеметом данных и центроидом каждого кластера
        double[] distances = new double[numClusters];

        for (int i = 0; i < data.length; ++i) {
            for (int k = 0; k < numClusters; ++k) {
                distances[k] = distance(data[i], means[k]);
            }

            // Для текущего элемента данных находит индекс кластера с минимальным расстоянием
            int newClusterID = minIndex(distances);

            //Назначаем новый кластер для элемента
            if (newClusterID != newClustering[i]) {
                changed = true;
                newClustering[i] = newClusterID;
            }
        }

        if (!changed) return false; // нет измения кластеров для всех элементов данных

        // Проверка количества элементов кластеров
        int[] clusterCounts = new int[numClusters];
        for (int i = 0; i < data.length; ++i) {
            int cluster = newClustering[i];
            ++clusterCounts[cluster];
        }

        for (int k = 0; k < numClusters; ++k) {
            if (clusterCounts[k] == 0) return false; // имеется кластер с нулевым числом элементов
        }

        System.arraycopy(newClustering, 0, clustering, 0, newClustering.length);

        return true; // нет пустых кластеров и хотябы один элемент перенесен в новый кластер
    }

    /**
     * Евклидово расстоние между двумя векторами
     *
     * @param firstVector  Первый вектор
     * @param secondVector Второй вектор
     */
    private double distance(double[] firstVector, double[] secondVector) {
        double sumSquaredDiffs = 0.0;
        for (int j = 0; j < firstVector.length; ++j) {
            sumSquaredDiffs += Math.pow((firstVector[j] - secondVector[j]), 2);
        }
        return Math.sqrt(sumSquaredDiffs);
    }

    /**
     * Индекс минимального элемента в массиве
     *
     * @param array Массив
     */
    private int minIndex(double[] array) {
        int indexOfMin = 0;
        double smallDist = array[0];
        for (int k = 0; k < array.length; ++k) {
            if (array[k] < smallDist) {
                smallDist = array[k];
                indexOfMin = k;
            }
        }
        return indexOfMin;
    }
}
