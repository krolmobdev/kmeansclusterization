/*
 * Created by JFormDesigner on Sun Jan 21 22:24:27 MSK 2018
 */

package kmeansclusterization;

import org.jfree.chart.ChartPanel;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * @author Oleg K
 */
public class MainForm extends JFrame {
    private SurveyData mSurveyData;
    private KMeansAlgorithm mClusterAlgorithm;
    private JFrame mVisualizationForm;

    public MainForm() {
        initComponents();
        setVisible(true);

        mSurveyData = new SurveyData();
    }

    private void buttonLoadSurveyDataActionPerformed(ActionEvent e) {
        // TODO add your code here
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new FileNameExtensionFilter("Excel file", "xls"));

        int ret = fileChooser.showDialog(null, "Выберите файл");

        String selectedFile = null;
        if (ret == JFileChooser.APPROVE_OPTION) {
            selectedFile = fileChooser.getSelectedFile().getAbsolutePath();
        }

        if (selectedFile != null) {
            try {
                mSurveyData.loadDataFromExcel(selectedFile);
                mClusterAlgorithm = null;

                if (mVisualizationForm != null) {
                    mVisualizationForm.dispose();
                }

                textAreaSurveyData.setText(mSurveyData.toString());
                textAreaClusterization.setText("");

                buttonClusterData.setEnabled(true);
                buttonVisualisedClusters.setEnabled(false);
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(this, "Ошибка загрузки данных:\n" + ex.getMessage(), Program.NAME, JOptionPane.ERROR_MESSAGE);
            }
        }

    }

    private void buttonClusterDataActionPerformed(ActionEvent e) {
        // TODO add your code here

        int clustersCount;
        try {
            clustersCount = Integer.parseInt(textFieldClusterCount.getText());
            if (clustersCount <= 0) {
                throw new Exception();
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, "Недопустимое число кластеров",
                    Program.NAME, JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (!mSurveyData.isEmpty()) {
            mClusterAlgorithm = new KMeansAlgorithm(clustersCount, mSurveyData.prepareClusteringData());
            mClusterAlgorithm.cluster();

            textAreaClusterization.setText(mClusterAlgorithm.getProtocol());
            buttonVisualisedClusters.setEnabled(true);
        } else {
            JOptionPane.showMessageDialog(this, "Не заданы данные для кластеризации",
                    Program.NAME, JOptionPane.ERROR_MESSAGE);
        }

    }

    private void buttonVisualisedClustersActionPerformed(ActionEvent e) {
        // TODO add your code here
        if (mClusterAlgorithm != null) {
            ClusterDataChart chart = new ClusterDataChart(mClusterAlgorithm.getClusteredData());

            if (mVisualizationForm != null) {
                mVisualizationForm.dispose();
            }

            mVisualizationForm = new JFrame();
            mVisualizationForm.getContentPane().add(new ChartPanel(chart.getChart()));
            mVisualizationForm.setSize(400, 400);
            mVisualizationForm.setLocationRelativeTo(null);
            mVisualizationForm.setVisible(true);

        }
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Oleg K
        buttonLoadSurveyData = new JButton();
        scrollPane1 = new JScrollPane();
        textAreaSurveyData = new JTextArea();
        labelSurveyData = new JLabel();
        scrollPane2 = new JScrollPane();
        textAreaClusterization = new JTextArea();
        labelClasterization = new JLabel();
        buttonClusterData = new JButton();
        labelClustersCount = new JLabel();
        textFieldClusterCount = new JTextField();
        buttonVisualisedClusters = new JButton();

        //======== this ========
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        setTitle("\u041a\u043b\u0430\u0441\u0442\u0435\u0440\u0438\u0437\u0430\u0446\u0438\u044f \u043e\u043f\u0440\u043e\u0441\u0430");
        Container contentPane = getContentPane();
        contentPane.setLayout(null);

        //---- buttonLoadSurveyData ----
        buttonLoadSurveyData.setText("\u0417\u0430\u0433\u0440\u0443\u0437\u0438\u0442\u044c \u0434\u0430\u043d\u043d\u044b\u0435");
        buttonLoadSurveyData.addActionListener(e -> buttonLoadSurveyDataActionPerformed(e));
        contentPane.add(buttonLoadSurveyData);
        buttonLoadSurveyData.setBounds(15, 315, 130, 25);

        //======== scrollPane1 ========
        {

            //---- textAreaSurveyData ----
            textAreaSurveyData.setEditable(false);
            textAreaSurveyData.setFont(new Font("Tahoma", Font.PLAIN, 11));
            scrollPane1.setViewportView(textAreaSurveyData);
        }
        contentPane.add(scrollPane1);
        scrollPane1.setBounds(15, 30, 245, 275);

        //---- labelSurveyData ----
        labelSurveyData.setText("\u0414\u0430\u043d\u043d\u044b\u0435 \u043e\u043f\u0440\u043e\u0441\u0430");
        contentPane.add(labelSurveyData);
        labelSurveyData.setBounds(15, 10, 115, labelSurveyData.getPreferredSize().height);

        //======== scrollPane2 ========
        {

            //---- textAreaClusterization ----
            textAreaClusterization.setEditable(false);
            textAreaClusterization.setFont(new Font("Tahoma", Font.PLAIN, 11));
            scrollPane2.setViewportView(textAreaClusterization);
        }
        contentPane.add(scrollPane2);
        scrollPane2.setBounds(285, 75, 240, 230);

        //---- labelClasterization ----
        labelClasterization.setText("\u041a\u043b\u0430\u0441\u0442\u0435\u0440\u0438\u0437\u0430\u0446\u0438\u044f");
        contentPane.add(labelClasterization);
        labelClasterization.setBounds(new Rectangle(new Point(285, 60), labelClasterization.getPreferredSize()));

        //---- buttonClusterData ----
        buttonClusterData.setText("\u041a\u043b\u0430\u0441\u0442\u0435\u0440\u0438\u0437\u043e\u0432\u0430\u0442\u044c");
        buttonClusterData.setEnabled(false);
        buttonClusterData.addActionListener(e -> buttonClusterDataActionPerformed(e));
        contentPane.add(buttonClusterData);
        buttonClusterData.setBounds(350, 30, 120, buttonClusterData.getPreferredSize().height);

        //---- labelClustersCount ----
        labelClustersCount.setText("\u0427\u0438\u0441\u043b\u043e \u043a\u043b\u0430\u0441\u0442\u0435\u0440\u043e\u0432");
        contentPane.add(labelClustersCount);
        labelClustersCount.setBounds(new Rectangle(new Point(285, 10), labelClustersCount.getPreferredSize()));
        contentPane.add(textFieldClusterCount);
        textFieldClusterCount.setBounds(285, 30, 55, textFieldClusterCount.getPreferredSize().height);

        //---- buttonVisualisedClusters ----
        buttonVisualisedClusters.setText("\u0412\u0438\u0437\u0443\u0430\u043b\u0438\u0437\u0438\u0440\u043e\u0432\u0430\u0442\u044c");
        buttonVisualisedClusters.setEnabled(false);
        buttonVisualisedClusters.addActionListener(e -> buttonVisualisedClustersActionPerformed(e));
        contentPane.add(buttonVisualisedClusters);
        buttonVisualisedClusters.setBounds(new Rectangle(new Point(285, 315), buttonVisualisedClusters.getPreferredSize()));

        { // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < contentPane.getComponentCount(); i++) {
                Rectangle bounds = contentPane.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = contentPane.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            contentPane.setMinimumSize(preferredSize);
            contentPane.setPreferredSize(preferredSize);
        }
        setSize(565, 380);
        setLocationRelativeTo(null);
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Oleg K
    private JButton buttonLoadSurveyData;
    private JScrollPane scrollPane1;
    private JTextArea textAreaSurveyData;
    private JLabel labelSurveyData;
    private JScrollPane scrollPane2;
    private JTextArea textAreaClusterization;
    private JLabel labelClasterization;
    private JButton buttonClusterData;
    private JLabel labelClustersCount;
    private JTextField textFieldClusterCount;
    private JButton buttonVisualisedClusters;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
