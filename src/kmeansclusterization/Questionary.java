package kmeansclusterization;

/**
 * Описывает анкету
 */
public class Questionary {
    private int mAge;//возраст
    private double mExperience;//стаж
    private double mAverageSalary;//средняя зарплата

    public Questionary() {
    }

    public Questionary(int age, int experience, int averageSalary) {
        this.mAge = age;
        this.mExperience = experience;
        this.mAverageSalary = averageSalary;
    }

    public int getAge() {
        return mAge;
    }

    public void setAge(int age) {
        this.mAge = age;
    }

    public double getAverageSalary() {
        return mAverageSalary;
    }

    public void setAverageSalary(double averageSalary) {
        this.mAverageSalary = averageSalary;
    }

    public double getExperience() {
        return mExperience;
    }

    public void setExperience(double experience) {
        this.mExperience = experience;
    }

    /**
     * Возвращает представление объекта в виде вещественного массива из 3 элементов: возраст, средняя зарплата,стаж
     */
    public double[] toDoubleArray() {
        double[] result = new double[3];

        result[0] = mAge;
        result[1] = mAverageSalary;
        result[2] = mExperience;

        return result;
    }

    @Override
    public String toString() {
        return "Возраст:" + mAge + " " +
                "Зарплата:" + mAverageSalary + " " +
                "Стаж:" + mExperience;
    }
}
