package kmeansclusterization;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Описывает данные опроса
 */
public class SurveyData {
    private List<Questionary> mSurvey = new ArrayList<Questionary>();//данные опроса

    /**
     * Возврашает данные опроса
     */
    public List<Questionary> getSurvey() {
        return mSurvey;
    }

    /**
     * Проверяет заданы ли данные опроса
     */
    public boolean isEmpty() {
        return mSurvey.size() == 0;
    }

    /**
     * Загружает данные опроса из файла
     *
     * @param filename Имя файла
     * @throws IOException При ошибке чтения из файла
     */
    public void loadDataFromExcel(String filename) throws IOException {
        List<Questionary> loadedData = new ArrayList<Questionary>();

        HSSFWorkbook workbook = new HSSFWorkbook(new FileInputStream(filename));
        HSSFSheet sheet = workbook.getSheet("данные");

        for (int r = 1; r <= sheet.getLastRowNum(); r++) {
            HSSFRow row = sheet.getRow(r);

            if (row == null) {
                break;
            }

            Questionary questionary = new Questionary();
            questionary.setAge((int) row.getCell(0).getNumericCellValue());
            questionary.setAverageSalary(row.getCell(7).getNumericCellValue());
            questionary.setExperience(row.getCell(1).getNumericCellValue());

            loadedData.add(questionary);
        }
        workbook.close();

        mSurvey = loadedData;
    }

    /**
     * Подготавливает данные для кластеризации
     */
    public double[][] prepareClusteringData() {
        double[][] clusteringData = new double[mSurvey.size()][3];

        int i = 0;
        for (Questionary questionary : mSurvey) {
            clusteringData[i] = questionary.toDoubleArray();
            i++;
        }

        return clusteringData;
    }

    @Override
    public String toString() {
        String result = "";
        StringBuilder sb = new StringBuilder(result);

        for (Questionary questionary : mSurvey) {
            sb.append(questionary.toString());
            sb.append('\n');
        }

        return sb.toString();
    }
}
