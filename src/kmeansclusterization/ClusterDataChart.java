package kmeansclusterization;

import org.jfree.chart.*;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import java.awt.*;
import java.util.List;
import java.util.Random;

/**
 * Диаграмма кластеров
 */
public class ClusterDataChart {
    private List<List<double[]>> mClustersData;

    public ClusterDataChart(List<List<double[]>> clustersData) {
        this.mClustersData = clustersData;
    }

    /**
     * Возвращает диаграмму, отображающую данные кластеров
     */
    public JFreeChart getChart() {

        //Формируем серии функций Андерсона для каждого элемента данных
        XYSeriesCollection seriesCollection = new XYSeriesCollection();

        int numSeries = 0;
        for (List<double[]> cluster : mClustersData) {
            for (double[] item : cluster) {
                XYSeries series = new XYSeries(numSeries);
                for (double i = (float) -Math.PI; i < Math.PI; i += 0.1) {
                    series.add(i, andrewsFunction(i, item));
                }
                seriesCollection.addSeries(series);
                numSeries++;
            }
        }

        JFreeChart chart = ChartFactory
                .createXYLineChart("Кластеризация", "", "",
                        seriesCollection,
                        PlotOrientation.VERTICAL,
                        true, true, true);

        //Задаем одинаковый цвет для серий, принадлежащих одному кластеру и легенду для каждого кластера
        LegendItemCollection legendItemCollection = new LegendItemCollection();

        numSeries = 0;
        int numCluster = 0;
        Random rand = new Random();
        for (List<double[]> cluster : mClustersData) {
            Color clusterColor = new Color(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255));
            LegendItem clusterLegend = new LegendItem("Кластер №" + numCluster, clusterColor);

            for (double[] item : cluster) {
                chart.getXYPlot().getRenderer().setSeriesPaint(numSeries, clusterColor);
                numSeries++;
            }
            legendItemCollection.add(clusterLegend);
            numCluster++;
        }
        chart.getXYPlot().setFixedLegendItems(legendItemCollection);

        return chart;
    }

    /**
     * Возвращает значение функции Андерсона (представление n-мерной точки в виде ряда Фурье)
     *
     * @param arg   агрумент функции
     * @param point точка в n-метном пространстве
     */
    private static double andrewsFunction(double arg, double[] point) {
        double value = 0.0;

        value += point[0] / Math.sqrt(2);
        int coeff = 1;
        for (int i = 1; i < point.length; i++) {
            if (i % 2 != 0) {
                value += point[i] * Math.sin(coeff * arg);
            } else {
                value += point[i] * Math.cos(coeff * arg);
                coeff++;
            }
        }

        return value;
    }
}
